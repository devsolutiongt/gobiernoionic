import { Platform } from '@ionic/angular';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { InAppBrowser, InAppBrowserObject } from '@ionic-native/in-app-browser/ngx';
import { AppAvailability } from '@ionic-native/app-availability/ngx';

@Component({
  selector: 'app-main',
  templateUrl: './main.page.html',
  styleUrls: ['./main.page.scss'],
  providers: [AppAvailability]
})
export class MainPage implements OnInit {

  schedule = [];

  constructor(public http: HttpClient, private router: Router, private route: ActivatedRoute, 
    private platform: Platform, private inAppBrowser: InAppBrowser, private appAvailability: AppAvailability) { }

  ngOnInit() {
    // Http Headers
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '*',
        'Authorization': 'YXBpX3Byb2dyYW1hY2lvbl9kZWxfZGlhOkRBUW9wRmZBUlNeKU5yalNZQnI5ZTleYg=='
      })
    };

    this.http.get<any>('https://gobiernodeguatemala.tv/wp-json/wp/v2/posts?filter[cat]=2&per_page=6&after=2020-12-02T01:00:00', httpOptions ).subscribe(data => {
      console.log('data: ' + data);
      console.log('data: ' + JSON.stringify(data));
      //this.router.navigate(['../dashboard', JSON.stringify(data)], { relativeTo: this.route });
      this.schedule = data;
    });
  }

  openFB() {
    this.launchApp(
      'fb://', 'com.facebook.katana',
      'fb://profile/guatemalagob',
      'fb://page/guatemalagob',
      'https://www.facebook.com/guatemalagob');
  }

  openTW() {
    this.launchApp(
      'twitter://', 'com.twitter.android',
      'twitter://user?screen_name=GuatemalaGob',
      'twitter://user?screen_name=GuatemalaGob',
      'https://twitter.com/GuatemalaGob');
  }

  openIG() {
    this.launchApp(
      'instagram://',
      'com.instagram.android',
      'instagram://user?username=guatemalagob',
      'instagram://user?username=guatemalagob',
      'https://www.instagram.com/guatemalagob');
  }

  openYT() {
    window.open('https://www.youtube.com/user/GobiernodeGuatemala', '_system');
  }
  
  private launchApp(iosApp: string, androidApp: string, appUrlIOS: string, appUrlAndroid: string, webUrl: string) {
    let app: string;
    let appUrl: string;
    // check if the platform is ios or android, else open the web url
    if (this.platform.is('ios')) {
      app = iosApp;
      appUrl = appUrlIOS;
    } else if (this.platform.is('android')) {
      app = androidApp;
      appUrl = appUrlAndroid;
    } else {
      const browser: InAppBrowserObject = this.inAppBrowser.create(webUrl, '_system');
      return;
    }
    this.appAvailability.check(app).then(
        () => {
            // success callback, the app exists and we can open it
            const browser: InAppBrowserObject = this.inAppBrowser.create(appUrl, '_system');
        },
        () => {
            // error callback, the app does not exist, open regular web url instead
            const browser: InAppBrowserObject = this.inAppBrowser.create(webUrl, '_system');
        }
    );
  }

}
