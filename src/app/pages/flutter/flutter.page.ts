import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpClientJsonpModule, HttpHeaders } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';

@Component({
  selector: 'app-flutter',
  templateUrl: './flutter.page.html',
  styleUrls: ['./flutter.page.scss'],
})
export class FlutterPage implements OnInit {

  notices = [];

  constructor(public http: HttpClient, private router: Router, private route: ActivatedRoute, private browser: InAppBrowser) { }

  ngOnInit() {

    // Http Headers
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '*',
        'Authorization': 'QXBwU2VjcmV0YXJpYTpoWXNXeGdpJGNPV0deWFJzeUFlKjBvbEU'
      })
    };

    this.http.get<any>('https://agn.gt/wp-json/wp/v2/posts?per_page=6&_embed', httpOptions ).subscribe(data => {
      console.log('data: ' + data);
      console.log('data: ' + JSON.stringify(data));
      //this.router.navigate(['../dashboard', JSON.stringify(data)], { relativeTo: this.route });
      this.notices = data;
    });
  }

  openUrl(url: string) {
    this.browser.create(url);
  }

}
