import { Component, OnInit } from '@angular/core';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.page.html',
  styleUrls: ['./menu.page.scss'],
})
export class MenuPage implements OnInit {

  home = [
    {
      title: 'Home',
      url: '/menu/main'
    }
  ];

  links = [
    {
      title: 'Presidencia',
      url: 'https://alejandrogiammattei.presidencia.gob.gt/'
    },
    {
      title: 'Vicepresidencia',
      url: 'https://vicepresidencia.gob.gt/'
    }
  ];

  pages = [
    {
      title: 'Ministerios',
      children: [
        {
          title: 'Ministerio de Comunicaciones, Infraestructura y Vivienda',
          url: 'http://www.civ.gob.gt/web/guest/83'
        },
        {
          title: 'Ministerio de Cultura y Deportes',
          url: 'http://mcd.gob.gt/'
        },
        {
          title: 'Ministerio de la Defensa Nacional',
          url: 'http://www.mindef.mil.gt/'
        },
        {
          title: 'Ministerio de Desarrollo Social',
          url: 'http://www.mides.gob.gt/webtwo/'
        },
        {
          title: 'Ministerio de Economía',
          url: 'https://www.mineco.gob.gt/'
        },
        {
          title: 'Ministerio de Educación',
          url: 'http://www.mineduc.gob.gt/portal/index.asp'
        },
        {
          title: 'Ministerio de Energía y Minas',
          url: 'https://mem.gob.gt/'
        },
        {
          title: 'Ministerio de Finanzas Públicas',
          url: 'https://www.minfin.gob.gt/'
        },
        {
          title: 'Ministerio de Gobernación',
          url: 'https://mingob.gob.gt/'
        },
        {
          title: 'Ministerio de Relaciones Exteriores',
          url: 'https://www.minex.gob.gt/'
        },
        {
          title: 'Ministerio de Salud Pública y Asistencia Social',
          url: 'https://www.mspas.gob.gt/'
        },
        {
          title: 'Ministerio de Trabajo y Previsión Social',
          url: 'https://www.mintrabajo.gob.gt/'
        }
      ]
    },
    {
      title: 'Secretarías',
      children: [
        {
          title: 'Secretaría General de la Presidencia',
          url: 'https://www.sgp.gob.gt/'
        },
        {
          title: 'Secretaría Privada de la Presidencia',
          url: 'http://www.secretariaprivada.gob.gt/'
        },
        {
          title: 'Secretaría de Coordinación Ejecutiva de la Presidencia',
          url: 'https://www.scep.gob.gt/'
        },
        {
          title: 'Secretaría de la Comunicación Social de la Presidencia',
          url: 'https://www.scspr.gob.gt/'
        },
        {
          title: 'Secretaría de Inteligencia Estratégica del Estado',
          url: 'https://www.sie.gob.gt/portal/'
        },
        {
          title: 'Secretaría de Planificación y Programación de la Presidencia',
          url: 'https://www.segeplan.gob.gt/nportal/'
        },
        {
          title: 'Secretaría de Asuntos Administrativos y de Seguridad de la Presidencia de la República',
          url: 'https://www.saas.gob.gt/'
        },
        {
          title: 'Secretaría de la Paz de la Presidencia de la República',
          url: 'http://www.sepaz.gob.gt/'
        },
        {
          title: 'Secretaría de Bienestar Social de la Presidencia',
          url: 'http://www.sbs.gob.gt/'
        },
        {
          title: 'Secretaría Presidencial de la Mujer',
          url: 'https://seprem.gob.gt/'
        },
        {
          title: 'Secretaría de Asuntos Agrarios de la Presidencia de la República',
          url: 'http://portal.saa.gob.gt/'
        },
        {
          title: 'Secretaría de Seguridad Alimentaria y Nutricional',
          url: 'http://www.sesan.gob.gt/'
        },
        {
          title: 'Secretaría Nacional de Ciencia y Tecnología',
          url: 'http://www.concyt.gob.gt/'
        },
        {
          title: 'Secretaría de Obras Sociales de la Esposa del Presidente de la República',
          url: 'http://www.sosep.gob.gt/'
        },
        {
          title: 'Secretaría Nacional de Administración de Bienes en Extinción',
          url: 'https://www.senabed.gob.gt/'
        },
        {
          title: 'Secretaría contra la Violencia sexual Explotación y Trata de Personas',
          url: 'https://www.svet.gob.gt/'
        },
        {
          title: 'Secretaría Técnica del Consejo Nacional de Seguridad República de Guatemala',
          url: 'https://stcns.gob.gt/'
        }
      ]
    },
    {
      title: 'Otras dependencias',
      children: [
        {
          title: 'Oficina Nacional de Servicio Civil',
          url: 'http://www.onsec.gob.gt/w1/'
        },
        {
          title: 'Fondo de Desarrollo Indígena Guatemalteco',
          url: 'https://fodigua.gob.gt/'
        },
        {
          title: 'Consejo Nacional de la Juventud',
          url: 'https://conjuve.gob.gt/'
        },
        {
          title: 'Defensoría de la Mujer Indígena',
          url: 'https://www.demi.gob.gt/'
        },
        {
          title: 'Autoridad para el Manejo Sustentable de la Cuenca del Lago de Atitlán y su Entorno',
          url: 'https://www.amsclae.gob.gt/'
        }
      ]
    },
    {
      title: 'Noticias',
      url: '/menu/flutter'
    }
  ];

  constructor(private browser: InAppBrowser) { }

  ngOnInit() {
  }

  openUrl(url: string) {
    this.browser.create(url);
  }

}
